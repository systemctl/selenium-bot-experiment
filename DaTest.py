#!/usr/env/python3

"""[Introduction]
This is a test about posting shit in twitter.

For this example we will use PRAW to get info from reddit and then post it at my twitter account

This code was made by Okami (okami@freejolitos.com)
"""

## Import section

import configparser                             # Parse data from config file
from selenium import webdriver                  # Selenium
from selenium.webdriver.common.keys import Keys # More Selenium
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep, strftime                # Timer
from random import randint                      # Random numbers
import pandas as pd                             # Nice pandas
import praw                                     # Reddit API
import numpy as np                              # The great numpy
from pprint import pprint                       # Pretty printer

## GLOBALS
webdriver = webdriver.Chrome(ChromeDriverManager().install())

"""[Reddit function]
Funtion to get info from reddit's subreddit
"""
def redditDownload(subredditName, limit, kind, thread):

    # Reddit credentials (readed from $HOME/.config/praw.ini)

    reddit = praw.Reddit("okami", user_agent='Wallpaper-API')

    # Reddit submission config

    subredditSelection = subredditName          # Subreddit's name
    subredditLimit = limit                      # Number of post that we want to download
    subredditKind = kind                        # Kind [hot, new, top, rising]
    subredditTimeline = thread                  # Timeline [all, year, month, week, day, now]

    rSubmission = reddit.subreddit(subredditSelection)

    if subredditKind == 'hot':
        rSubmissionThread = rSubmission.hot(subredditTimeline, limit=subredditLimit)
    elif subredditKind == 'top':
        rSubmissionThread = rSubmission.top(subredditTimeline, limit=subredditLimit)
    elif subredditKind == 'rising':
        rSubmissionThread = rSubmission.rising(subredditTimeline, limit=subredditLimit)
    else:
        rSubmissionThread = rSubmission.new(limit=subredditLimit)

    # Empty array for the results

    subredditObject = {
        "title": [],
        "subreddit": [],
        "score": [],
        "id": [],
        "url": [],
        "comms_num": [],
        "created": [],
        "body": []
    }

    # Downloading  =)

    for submission in rSubmissionThread:

        # Check if subreddit title fits in twitter char lenght
        if len(submission.title) < 250:
            subredditObject["title"].append(submission.title)
            subredditObject['subreddit'].append(submission.subreddit)
            subredditObject["score"].append(submission.score)
            subredditObject["id"].append(submission.id)
            subredditObject["url"].append(submission.url)
            subredditObject["comms_num"].append(submission.num_comments)
            subredditObject["created"].append(submission.created)
            subredditObject["body"].append(submission.selftext)
    return subredditObject

"""[Twitter function]
This funcion make tweets using selenium
"""

def tweetingMachine(subreddits):

    # Getting Twitter credentials from ini file
    config = configparser.ConfigParser()
    config.read('config.ini')  # Needs relative path
    sections = config.sections()
    print(f'Sections: {sections}')

    twAccountData = config['twaccount']
    twUsername = twAccountData['twUsername']        # Username
    twPassword = twAccountData['twPassword']        # Password

    # Path to browser driver
    #chromedriver_path = './chromedriver'
    #webdriver = webdriver.Chrome(executable_path=chromedriver_path)
    sleep(2)

    # Navigate Twitter login
    webdriver.get('https://twitter.com/login')
    sleep(2)

    # Action login with provided credentials
    username = webdriver.find_element_by_name('session[username_or_email]')
    username.send_keys(twUsername)
    password = webdriver.find_element_by_name('session[password]')
    password.send_keys(twPassword)
    button_login = webdriver.find_element_by_css_selector('#react-root > div > div > div.css-1dbjc4n.r-13qz1uu.r-417010 > main > div > div > form > div > div:nth-child(8) > div > div > span > span') 
                                                        #  #react-root > div > div > div.css-1dbjc4n.r-13qz1uu.r-417010 > main > div > div > form > div > div:nth-child(8) > div > div > span > span
    sleep(randint(2,10))
    button_login.click()
    sleep(3)

    # Make the tweet happen
    for ii in subreddits["id"]:
        sIndex = subreddits["id"].index(ii)
        print('Titulo: ' + subreddits["title"][sIndex])

        # Navigate to composing tweets address
        webdriver.get('https://twitter.com/compose/tweet')
        sleep(randint(8,17))

        # Writting the tweet
        fakeTweet = webdriver.find_element_by_xpath('//*[@id="react-root"]/div/div/div[1]/div[2]/div/div/div/div[2]/div[2]/div/div[3]/div/div/div/div[1]/div/div/div/div/div[2]/div[1]/div/div/div/div/div/div/div/div/div/div[1]/div/div/div/div[2]/div/div/div/div')
        fakeTweet.send_keys(subreddits["title"][sIndex])
        sleep(randint(4,19))

        fakeTweet.send_keys(Keys.COMMAND + Keys.ENTER)
        sleep(randint(2,12))

        sleep(15)
        return

def main():

    # TODO: obtener estos datos del archivo de configuración
    subredditName = 'quotes'
    limit = 10
    kind = 'top'
    thread = 'day'

    subreddits = redditDownload(subredditName, limit, kind, thread)

    tweetingMachine(subreddits)


if __name__ == '__main__':
    main()
